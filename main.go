package main

import (
	"log"
	"os"
	"runtime/debug"

	"gitlab.com/Iver14/ping/lib"
)

func main() {
	defer func() {
		if r := recover(); r != nil {
			log.Printf("| Fatal Error | %v", r)
			log.Printf("| Fatal Error | Stack:  %v", string(debug.Stack()))
		}
	}()

	var app *lib.App
	var err error
	if app, err = lib.NewApp(); err == nil {
		app.Parse(os.Args[1:])
	} else {
		log.Printf("Error creating app: %v", err)
		os.Exit(1)
	}
}
