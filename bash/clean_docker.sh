#!/usr/bin/env bash

process() {
  docker rm $(docker ps -a -q)
}
images() {
  docker rmi $(docker images -q)
}
volumes() {
  docker volume rm $(docker volume ls |awk '{print $2}')
}
data(){
  rm -rf ~/Library/Containers/com.docker.docker/Data/*
}

process
images
volumes
data
