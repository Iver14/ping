// Package lib contains all project logic.
package lib

import (
	"flag"
	"fmt"

	"gopkg.in/alecthomas/kingpin.v2"
)

const (
	appName = "Ping"
	desc    = "Test connection to mongo"
	hostMsg = "Hosts to connect to"
	userMsg = "User for authentication"
	passMsg = "Password for authentication"
	dataMsg = "Database to connect to"
	sslMsg  = "If true SSL is enabled"
	crtMsg  = "File with certificate data"
)

var (
	hash = "5fab2d8"
	log  *Logger
	// Version is a variable
	version = "Ping version v0.0.1"
	logFile = "logs/error.log"
)

// App models current application
type App struct {
	app     *kingpin.Application
	Log     *Logger
	Session *Session
}

func init() {
	var err error
	log, err = GetLogger(logFile, true);
	printError(err, "init().GetLogger()" )
}

func printError(err error, tag string) {
	if err != nil && flag.Lookup("test.v") == nil {
		fmt.Printf("| Error | %v: %#v \n", tag, err)
	}
}

// NewApp provides a new App struct with its initializated fields
func NewApp() (application *App, err error) {
	application = &App{
		app: kingpin.New(appName, desc),
		Log: log,
	}
	kingpin.Version(application.Version())
	application.required()
	return
}

// Version function returns the application version
func (a *App) Version() (v string) {
	log.Debugf("Version is %v, %v", version, hash)
	v = fmt.Sprintf("%v %s", version, hash)
	a.app.Version(v)
	return
}

// Parse function process the arguments
func (a *App) Parse(args []string) {
	kingpin.MustParse(a.app.Parse(args))
}

func (a *App) required() {
	var uri = &URI{}
	a.Session = &Session{}
	a.app.Flag("host", hostMsg).Short('h').Action(a.Connect)
	a.app.Flag("user", userMsg).Short('u').StringVar(&uri.User)
	a.app.Flag("password", passMsg).Short('p').StringVar(&uri.Password)
	a.app.Flag("database", dataMsg).Short('d').Default("local").StringVar(&uri.Database)
	a.app.Flag("ssl", sslMsg).Short('s').Action(a.Secure).BoolVar(&uri.SSL)
	a.app.Flag("certificate", crtMsg).Short('c').Default("ssl/client.pem").FileVar(&uri.Certificate)
	a.Session.URI = uri
}

func CheckHost(ctx *kingpin.ParseContext) (isOK bool) {
	log.Debugf("Commands: %#v", ctx.SelectedCommand)
	log.Debugf("Elements: %#v", ctx.Elements)
	isOK = true
	return
}

// Connect send a ping to mongo
func (a *App) Connect(ctx *kingpin.ParseContext) error {
	CheckHost(ctx)
	log.Debugf("Session OK: %v \n", a.Session.URI.String())
	sess, err := a.Session.Open()
	defer sess.Close()
	if err != nil {
		printError(err, "Connect()")
		return err
	}
	err = sess.Ping()
	if err != nil {
		printError(err, "Ping()")
		return err
	}
	return nil
}

// Secure connection happens
func (a *App) Secure(ctx *kingpin.ParseContext) (err error) {
	fmt.Printf("Secure connection %#v \n", a.Session.URI.Certificate)
	fmt.Printf("Session OK: %v \n", a.Session.URI.String())
	sess, err := a.Session.OpenSecure()
	defer sess.Close()
	if err != nil {
		printError(err, "Secure()")
		return err
	}
	err = sess.Ping()
	if err != nil {
		printError(err, "Ping()")
		return err
	}
	return nil
}
