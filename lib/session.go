package lib

import (
	"crypto/tls"
	"net"
	"os"
	"time"

	mgo "gopkg.in/mgo.v2"
)

// Session models each databse session
type Session struct {
	Certificate *os.File
	URI         *URI
	sess        *mgo.Session
}

// Open a session from mongo server
func (s *Session) Open() (sess *mgo.Session, err error) {
	var info *mgo.DialInfo
	log.Debug("1 - Open() ... ")

	if info, err = mgo.ParseURL(s.URI.String()); err != nil {
		printError(err, "ParseURL")
		return nil, err
	}
	info.Timeout = time.Duration(5)
	log.Debugf("2 - Open() ... DialInfo: %#v \n", info)
	if sess, err = mgo.DialWithInfo(info); err != nil {
		printError(err, "Open()")
	}
	log.Debug("4 - Open() ... ")
	defer sess.Close()
	log.Debug("5 - Open() ... ")
	return sess.Clone(), err
}

// OpenSecure create a secure session from mongo server
func (s *Session) OpenSecure() (sess *mgo.Session, err error) {
	var info *mgo.DialInfo
	/*	log.Debug("1 - OpenSecure() ...")
		sess, err = mgo.DialWithTimeout(s.URI.String(), time.Duration(5))
		if err != nil {
			log.Debugf("| ERROR | OpenSecure() ... %#v \n", err)
		}*/
	if info, err = mgo.ParseURL(s.URI.String()); err != nil {
		printError(err, "ParseURL")
		return nil, err
	}
	log.Debug("2 - OpenSecure() ... ")
	info.Timeout = time.Duration(5)
	info.Mechanism = "MONGODB-X509"
	info.DialServer = func(addr *mgo.ServerAddr) (conn net.Conn, err error) {
		log.Debug("<--- Connection -->")

		var tlsConfig *tls.Config
		if tlsConfig, err = ConfigureCert(s.URI.Certificate); err != nil {
			printError(err, "ConfigureCert()")
			return nil, err
		}
		conn, err = tls.Dial("tcp", addr.String(), tlsConfig)

		if err != nil {
			printError(err, "tls.Dial()")
		}
		return conn, err

	}
	log.Debugf("3 - OpenSecure() ... Info: %#v \n", info)
	if sess, err = mgo.DialWithInfo(info); err != nil {
		printError(err, "DialWithInfo()")
	}
	log.Debug("4 - OpenSecure() ... ")
	cloned := sess.Clone()
	log.Debug("5 - OpenSecure() ... ")
	defer sess.Close()
	err = sess.Ping()
	if err != nil {
		printError(err, "DialWithInfo()")
		return nil, err
	}
	return cloned, err
}

// Connection returns net.Conn established with mgo.ServerAddr info
func (s *Session) Connection(addr *mgo.ServerAddr) (conn net.Conn, err error) {
	log.Debug("<--- Connection -->")

	var tlsConfig *tls.Config
	if tlsConfig, err = ConfigureCert(s.URI.Certificate); err != nil {
		log.Debugf("ConfigureCert() ...  %#v \n", err)
		return nil, err
	}
	conn, err = tls.Dial("tcp", addr.String(), tlsConfig)

	if err != nil {
		log.Debugf("Open() ... configure() Dial() %#v \n", err)
	}
	return conn, err

}
