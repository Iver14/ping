package lib

import (
	"fmt"
	"os"
	"strings"
)

// URI models connection string URI
type URI struct {
	Certificate *os.File
	Database    string
	Hosts       []string
	Password    string
	User        string
	SSL         bool
	Options []string
}

func (u *URI) String() (connString string) {
	hosts := strings.Join(u.Hosts, ",")
	var data = []string{
		u.protocol(),
		u.auth(),
		hosts,
		"/", u.Database,
		u.options(),
	}
	connString = strings.Join(data, "")
	return
}

func (u *URI) options() (options string) {
	if len(u.Options) > 0 {
		options = fmt.Sprintf("?%v", strings.Join(u.Options, "&"))
	}
	return
}

func (u *URI) protocol() (protocol string) {
	protocol = "mongodb://"
	if u.SSL {
		protocol = "mongodb+srv://"
	}
	return
}
func (u *URI) auth() (data string) {
	data = ""
	if u.User != "" && u.Password != "" {
		data = fmt.Sprintf("%v:%v@", u.User, u.Password)
	}
	return
}
