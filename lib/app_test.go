package lib_test

import (
	"testing"

	"gitlab.com/Iver14/ping/lib"
)

func TestNewApp(t *testing.T) {
	app, err := lib.NewApp()

	if err != nil {
		t.Errorf("NewApp gets error: %v", err)
	}

	if app == nil {
		t.Error("NewApp returns nil")
	}

	if app.Log == nil {
		t.Error("app.Log is nil")
	}
}
