package lib

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"

	"os"
)

const rootPEM = `
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAuN2lH/KPz+uZ+juYuOwCVEOUvGeH5TYfE0T4SIfBzqheBMp+
LrilytL2vqWSGt4IY3jf+eTH1sm86zV+VaP9fwyXLWJxud5HBo35KDTaT7CZFmQ4
LUUlMWjyXnY9Ff2aIQQsE5XR3Inzg0Pr5RMYPlvq6ssRvZ8PVp5GDrRfBhq7pClU
O1rP+gjrosg4ajcrNz1sihe4+oLE6O9ZMfelS+4uHSIVB3ha9QHpymHbW/ILKrbU
67ejYP7/VlLCOS8TQK+2T5cl7T8kSPyngR7OmD/Db2Mwdv5bhfcY1Hoy++xqO6Ao
XW0/CbNuyk1IgsQkabAoQPQDvxAMoRDkisnPGwIDAQABAoIBAQCYhz/r2R2xRsoX
vb5nrUrKrav1u1JT21DOfFSCu4g7840HVO/BqtYYP/IYINIo2tN/B0d9jrcX0AHQ
66UeEh1hC0lcx+HfordlgBR4iQsrPyFAaVY0e92RI+79437JOyJFZYTcb/vkg+no
f00OB7A0G3D7Yqtzkd588EALd+UDSfn4+mkYAi0zs9+EQTLihdg47IO3FSk/crq5
fU5XievTgZuiNuuIcvbwEaDUwynSfU4YZEevfOfdsFOnkdzfjlmERrlEK5/K0fNH
793ozUFU9YHe7az0EnWJCM2RVP4Lh22xTbUOFHtZly019VCdeDNDBXst8CN4kd6L
zaz5pLiBAoGBAOOF93NI0FasIloZ1HeFtmVAs4f+rE6O09KfLa8cGbaPCg93MpjA
OE/6/w498hGY1WELmfCScOCL1eF+wanIxMWKxgCOWuF/9ijlklg7fYZfr4QpKF00
M5CQGtH3nl6rdxZA8gzT6hZ+nHUPcgyjbJIhJHD8Bzt8rRkIx+D4Jr9BAoGBANAA
5Zzk3yCp75YcyvKSQ5oiNGgOmWrwQ5NUlXOBAi+5YEvLst4/v6VOdaurGKZbDPGa
H5q54e/W1yPMYIiCKcnRgOc/hQ2f8s/QYogoGxsENj5Q4SSnpo2kw6tqLRZ5Z0QY
xkBB+VIcSOU0tgnGT/9sT2iukzFBEZsFid2yUhNbAoGAC73AFUzuo2pio2+tzTUz
QmpiWWRFFBC+0aGxQ9YZZZhgFog6LjPNNCNpwhxkP5A5graMjSOhe5T7xpU5/T8F
GISezyyJeBWYaGfgc3kucx7A+fr9wEGPX6hwGeHDZHNIw/c6BntxGxQRkuDnK8vL
MIdRUBtRbDp03FPdrLhhl4ECgYAL0cE6ZRb4uC9RDFnL5tvIimEH+KxnEzgDyy6i
3ZXOOG5sYCrNzjmJnye4lYRVD59wqx1YoW9hPYgEFLz3hM3zFeCJQ2bAG3KktGak
7gJ8SoT6jYX9mzkvm0kzGFlviX6AtBev1w/fxpOt0oz1iTtKfFEW1bSve88QTARF
gLiBOQKBgQC+iAdpclxHOnHb4usbdrsieZHhggQzakjgufHEaH/6r/PB5XK0ukBa
loVq12+iVxkyzmmBx+qZxjW/iRtQdl/B0HWuWY70EqAZi4MUl4YoMV6cFx6WCtWW
ero7yEw1SVEsELPMrQH48hLyy8336zFerD50aH5IpN0/aPGS9yNu3w==
-----END RSA PRIVATE KEY-----
-----BEGIN CERTIFICATE-----
MIICrTCCAZUCCQCtE6cPmQosvjANBgkqhkiG9w0BAQUFADAgMR4wHAYDVQQDDBVD
ZXJ0aWZpY2F0ZSBBdXRob3JpdHkwHhcNMTkwNjIxMTY1MTA0WhcNMjkwNjE4MTY1
MTA0WjARMQ8wDQYDVQQDDAZDbGllbnQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw
ggEKAoIBAQC43aUf8o/P65n6O5i47AJUQ5S8Z4flNh8TRPhIh8HOqF4Eyn4uuKXK
0va+pZIa3ghjeN/55MfWybzrNX5Vo/1/DJctYnG53kcGjfkoNNpPsJkWZDgtRSUx
aPJedj0V/ZohBCwTldHcifODQ+vlExg+W+rqyxG9nw9WnkYOtF8GGrukKVQ7Ws/6
COuiyDhqNys3PWyKF7j6gsTo71kx96VL7i4dIhUHeFr1AenKYdtb8gsqttTrt6Ng
/v9WUsI5LxNAr7ZPlyXtPyRI/KeBHs6YP8NvYzB2/luF9xjUejL77Go7oChdbT8J
s27KTUiCxCRpsChA9AO/EAyhEOSKyc8bAgMBAAEwDQYJKoZIhvcNAQEFBQADggEB
AHK0kMHJzHkWZJMDmvbeS1HTAwSIhx1RCh4MGuvTKBLN4WJiBFzOko46rRmOczLZ
BCaW82TN+LeTUF7ANncB6nPEjLYZ/dWVQPy3VhOa1723MxR7tS0NCree3ugP0YFf
hIypOWJ1mRKjavzrwHe/St16uGfKgnzzIxgUp7pjO3Oo9xhpb27wIPkB7eSYToqA
1xKlGNrif+fUcbZqYMOcJ0oDjvMq3ot5oCqUt0x1D/vWXTGeV0Ik1mFbSiBwKLLu
Fp5WMqPGuBflGiNQ+NNARdc8f2+3Y5Rv3f+8qsFBKfCxEGH+rhsZqp4XITVpjwHy
wFRcKul8wjdHcHydAvechsg=
-----END CERTIFICATE-----`

// ConfigureCert returns tls configured struct
func ConfigureCert(file *os.File) (config *tls.Config, err error) {
	roots := x509.NewCertPool()
	fmt.Println("Open() ... configure()")

	/*var bytes []byte
	if bytes, err = ioutil.ReadFile(file.Name()); err != nil {
		return
	}*/

	var bytes = []byte(rootPEM)

	if roots.AppendCertsFromPEM(bytes) {
		fmt.Printf(" Root with cert %#v \n", roots)
	} else {
		fmt.Printf("| ERROR | ConfigureCert() ... %#v \n", err)
	}
	config = &tls.Config{
		InsecureSkipVerify: true,
		RootCAs:            roots,
	}
	return
}
