package lib_test

import (
	"fmt"
	"gitlab.com/Iver14/ping/lib"
	"gotest.tools/assert"
	"strings"
	"testing"
	"os"
)

type ListTest []URITest

type URITest struct {
	Data *lib.URI
	Expected string
	Tag string
}

var testList ListTest

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}

func setup(){
	fmt.Println("--- setup")
	testList = ListTest{}

	emptyList := []string{}
	optList := []string{"authSource=authSourcedb"}

	base := "mongodb://user:pass@host/database"
	noAuth := "mongodb://host/database"
	noDB := "mongodb://user:pass@host/"
	noDBNoAuth := "mongodb://host/"
	withPort := "mongodb://user:pass@host:2020/database"
	withPortNoDB := "mongodb://user:pass@host:2020/"
	sslEnabled := "mongodb+srv://user:pass@host/database"
	sslNoAuth := "mongodb+srv://host/database"
	withMultiNode := "mongodb://user:pass@host1,host2/database?authSource=authSourcedb"
	withMultiNodePort := "mongodb://user:pass@host1:33719,host2:33719/database?authSource=authSourcedb"
	withMultiNodeSSL := "mongodb+srv://user:pass@host1,host2/database?authSource=authSourcedb"
	withMultiNodeSSLPort := "mongodb+srv://user:pass@host1:33719,host2:33719/database?authSource=authSourcedb"

	testList = appendURI(testList, newURI( []string{"host"}, "user", "pass", "database", false, emptyList), base, "base")
	testList = appendURI(testList, newURI( []string{"host"}, "", "", "database", false, emptyList), noAuth, "noAuth")
	testList = appendURI(testList, newURI( []string{"host"}, "user", "pass", "", false, emptyList), noDB, "noDB")
	testList = appendURI(testList, newURI( []string{"host"}, "", "", "", false, emptyList), noDBNoAuth, "noDBNoAuth")
	testList = appendURI(testList, newURI( []string{"host:2020"}, "user", "pass", "database", false, emptyList), withPort, "withPort")
	testList = appendURI(testList, newURI( []string{"host:2020"}, "user", "pass", "", false, emptyList), withPortNoDB, "withPortNoDB")
	testList = appendURI(testList, newURI( []string{"host"}, "user", "pass", "database", true, emptyList), sslEnabled, "sslEnabled")
	testList = appendURI(testList, newURI( []string{"host"}, "", "", "database", true, emptyList), sslNoAuth , "sslNoAuth")
	testList = appendURI(testList, newURI( []string{"host1","host2"}, "user", "pass", "database", false, optList), withMultiNode , "withMultiNode")
	testList = appendURI(testList, newURI( []string{"host1:33719","host2:33719"}, "user", "pass", "database", false, optList), withMultiNodePort, "withMultiNodePort")
	testList = appendURI(testList, newURI( []string{"host1","host2"}, "user", "pass", "database", true, optList), withMultiNodeSSL , "withMultiNodeSSL")
	testList = appendURI(testList, newURI( []string{"host1:33719","host2:33719"}, "user", "pass", "database", true, optList), withMultiNodeSSLPort, "withMultiNodeSSLPort")
}

func newURI( host []string, user, pass, db string, ssl bool, opts []string) (uri *lib.URI) {
	return &lib.URI{
		Hosts:    host,
		Database: db,
		Password: pass,
		User:     user,
		SSL:      ssl,
		Options: opts,
	}
}

func appendURI(source ListTest, uri *lib.URI, expected, tag string) (list ListTest) {
   list = append(source, URITest{
		Data: uri,
		Expected: expected,
		Tag: tag,
	})
   return
}

func shutdown(){
	fmt.Println("--- shutdown")
}

func TestURI_String(t *testing.T){
	for _, element := range testList {
		result := element.Data.String()
		expected := element.Expected
		t.Logf("Result: %#v - Tag: %v", result, element.Tag)
		t.Logf("Expected: %#v", expected)
		assert.Assert(t, strings.Compare(result, expected) == 0)
	}

}